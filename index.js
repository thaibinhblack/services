const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
var cors = require('cors')
const { exec } = require("child_process");
var schedule = require('node-schedule');

var rule = new schedule.RecurrenceRule();
rule.minute = 1;
schedule.scheduleJob(rule, function(){
  console.log('running job!');
  exec("php /home/api.ecosyco.com/public_html/artisan schedule:run", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
  });
  
});
